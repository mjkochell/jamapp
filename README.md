# JamApp

This app connects musicians' devices together and allows them to have real-time communication for which chords to play at a given time. It uses Angular4 for client-side, and a NodeJS REST service and WebSocket service for server-side.