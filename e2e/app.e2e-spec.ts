import { JamAppPage } from './app.po';

describe('jam-app App', () => {
  let page: JamAppPage;

  beforeEach(() => {
    page = new JamAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
