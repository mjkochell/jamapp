import { WebsocketIncomingActionService } from './web-socket/websocket-incomingaction.service';
import { WebSocketService } from './web-socket/web-socket.service';
import { APIService } from './mycore/api';
import { CollectionService } from './progression/collection.service';
import { Component, ViewChild, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { NotationComponent } from './notation/notation.component';
import { QuizInfoComponent } from './quiz-info/quiz-info.component';
import { NoteInfoComponent } from './note-info/note-info.component';

import { PianoService } from './core/piano.service';
import { SoundService } from './core/sound.service';
import { PianoNote } from './core/piano-note';
import { PianoMode } from './core/piano-mode.enum';
import { QuizStatus } from './core/quiz-status.enum';

@Component({
   selector: 'app-root',
   templateUrl: './app.component.html',
   styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  PianoMode = PianoMode; // allows template access to PianoMode enum
  title = 'Piano Play';
  mode: PianoMode = PianoMode.Play;
  subscription: Subscription;
  private username: string;

  private currentTestNote: PianoNote;
  private timeoutId: any;
  private delayMs = 1000;

  @ViewChild(NotationComponent) notation: NotationComponent;

  constructor(
    private pianoService: PianoService,
    private soundService: SoundService,
    private collectionService: CollectionService,
    private apiService: APIService,
    private wsService: WebSocketService,
    private wsIncoming: WebsocketIncomingActionService) {
      this.subscription = pianoService.notePlayed$.subscribe(note => this.handleNotePlayed(note));
      let s = this.apiService.getSessionShorts().subscribe(data => {
        s.unsubscribe();
      });
      this.username = '';
  }



  ngOnInit() {
    this.soundService.initialize();
    this.wsService.connect();
  }

  handleModeSelected(selectedMode: PianoMode) {

      // this.notation.clear();
    
  }

  handleKeyPlayed(keyId: number) {
      this.soundService.playNote(keyId);
  }

  handleNotePlayed(note: PianoNote) {
    this.soundService.playNote(note.keyId);
  }

}
