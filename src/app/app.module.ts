import { ScalesService, ScaleCreator } from './progression/scales.service';
import { WebsocketIncomingActionService } from './web-socket/websocket-incomingaction.service';
import { WebsocketOutgoingActionService } from './web-socket/websocket-outgoingaction.service';

import { ChordtypenamePipe } from './mycore/pipes/chordtypename.pipe';
import { RootnamePipe } from './mycore/pipes/rootname.pipe';
import { WebSocketService } from './web-socket/web-socket.service';
import { ProgressionService } from './progression/progression.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { KeyboardComponent } from './keyboard/keyboard.component';
import { NotationComponent } from './notation/notation.component';
import { PlayControlComponent } from './play-control/play-control.component';
import { NoteInfoComponent } from './note-info/note-info.component';
import { QuizInfoComponent } from './quiz-info/quiz-info.component';
import { PianoService } from './core/piano.service';
import { QuizService } from './core/quiz.service';
import { SoundService } from './core/sound.service';
import { NotationService } from './notation/notation.service';
import { SafePipe } from './shared/safe.pipe';
import { ProgressionViewComponent } from './progression/progression-view/progression-view.component';
import { ButtonPanelComponent } from './button-panel/button-panel.component';
import { APIService } from './mycore/api';
import { CollectionService } from 'app/progression/collection.service';
import { EditProgComponent } from './routes/editprog/editprog.component';
import { SelectProgComponent } from './routes/selectprog/selectprog.component';
import { LoginComponent } from './routes/login/login.component';
import { SelectMiniProgComponent } from './routes/selectminiprog/selectminiprog.component';
import { CurrentViewService } from 'app/routes/current-view.service';

import { ConfigService } from 'app/mycore/config.service';
import { SessionContextService } from 'app/mycore/session-context.service';
import { ViewprogComponent } from './routes/viewprog/viewprog.component';

import {Ng2Webstorage} from 'ng2-webstorage';



const routes: Routes = [
   { path: '', pathMatch: 'full', redirectTo: 'login' },
   { path: 'login', component: LoginComponent },
   { path: 'editprog', component: EditProgComponent },
   { path: 'selectprog', component: SelectProgComponent },
   { path: 'viewprog', component: ViewprogComponent },
  //  { path: 'second', component: SecondComponent },
  //  { path: 'third', component: ThirdComponent },
 ];

@NgModule({
  declarations: [
    AppComponent,
    KeyboardComponent,
    NotationComponent,
    PlayControlComponent,
    NoteInfoComponent,
    QuizInfoComponent,
    SafePipe,
    ProgressionViewComponent,
    ButtonPanelComponent,
    EditProgComponent,
    SelectProgComponent,
    LoginComponent,
    SelectMiniProgComponent,
    RootnamePipe,
    ChordtypenamePipe,
    ViewprogComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CommonModule,
    RouterModule.forRoot(routes),
    Ng2Webstorage,
  ],
  providers: [
    PianoService,
    SoundService,
    NotationService,
    QuizService,
    ProgressionService,
    WebSocketService,
    CollectionService,
    APIService,
    CurrentViewService,
    WebsocketIncomingActionService,
    WebsocketOutgoingActionService,
    ConfigService,
    SessionContextService,
    ScalesService,
    ScaleCreator,
  ],
  exports: [RouterModule],
  bootstrap: [AppComponent]
})
export class AppModule { }

  // constructor(private wsService: WebSocketService,
  //             private config: ConfigService,
  //             private progService: ProgressionService,
  //             private currentView: CurrentViewService,
  //             private progCollection: CollectionService) {
