import { ScalesService } from './../progression/scales.service';
import { ChordType } from './../mycore/model';
import { ProgressionService } from './../progression/progression.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-button-panel',
  templateUrl: './button-panel.component.html',
  styleUrls: ['./button-panel.component.scss']
})
export class ButtonPanelComponent implements OnInit {
  private chordTypes: any;


  
  constructor(private progService: ProgressionService, private scalesService: ScalesService) {
    this.chordTypes = ChordType;
  }

  setChordType(chordType: ChordType) {
    this.progService.setChordType(chordType);
  }

  ngOnInit() {
  }

  toggleScaleMode() {
    this.scalesService.toggleScaleMode();
  }

}
