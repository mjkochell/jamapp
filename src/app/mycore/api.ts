import { SessionContextService } from 'app/mycore/session-context.service';
import { Observable } from 'RxJS/Rx';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class APIService {

    constructor(private http: Http, private context: SessionContextService) {

    }

    
    getSessionShorts(): Observable<any> {
        return this.http.get('sessions/short')
            .map(res => res.json());

    }

    getSession(idsession) {
        return this.http.get(`sessions/${idsession}`)
            .map(res => res.json());
    }

    copyProg(prog) {
        let body = {
            prog,
            idsocket: this.context.getidSocket(),
        };
        let idsession = this.context.getidSession();

        return this.http.post(`sessions/${idsession}/copyProg`, body)
            .map(res => res.json());
    }

    createSession() {

    }

    createProgression() {
        let body = {};
        let idsession = this.context.getidSession();
        return this.http.post(`sessions/${idsession}/addProgression`, {})
            .map(res => res.json());
    }

    updateProgression(prog) {
        let idprog = prog.idprog;
        return this.http.put(`progressions/${idprog}`, prog)
            .map(res => res.json()).subscribe(data => {
               
        });

    }


}
