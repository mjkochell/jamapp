import { PianoService } from './../core/piano.service';
import { PianoNote } from './../core/piano-note';

export enum ChordType {
  Major,
  Minor,
  None,
}

export interface IChordProgression {
  chords: IChord;
}

export class ChordProgression {
  chords: Chord[];
  idsession: number;
  idprog: number;

  constructor(prog, pianoService?: PianoService) {
    if (!prog) {
      this.chords = [];
    } else {
      this.chords = prog.chords.map(chord => {
        if (chord.typeStr) {
          let note = pianoService.getNoteByKeyId(chord.root.keyId);
          return new Chord({root: note, type: chord.type});  
        }
        let note = pianoService.getNoteByKeyId(chord.root);
        return new Chord({root: note, type: chord.chordType});
    });
    
      this.idsession = prog.idsession;
      this.idprog = prog.idprog;
    }
  }

  addChord(chord: Chord) {
    this.chords.push(chord);
  }
}

export interface IChord {
  root: PianoNote;
  type: ChordType;
}

export class Chord implements IChord {
  root: PianoNote;
  type: ChordType;
  typeStr: string;
  chordType: ChordType;

  constructor(initial: IChord) {
    this.root = initial.root;
    this.type = initial.type;
    this.chordType = initial.type;
    this.typeStr = ChordType[this.type];
    
  }

  setRoot(note: PianoNote) {
    this.root = note;
  }

  setChordType(chordType: ChordType) {
    this.type = chordType;
    this.typeStr = ChordType[this.type];
  }
}
