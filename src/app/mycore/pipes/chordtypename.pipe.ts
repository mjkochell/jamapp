import { Pipe, PipeTransform } from '@angular/core';
import { ChordType } from 'app/mycore/model';


@Pipe({
  name: 'chordtypename'
})
export class ChordtypenamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return ChordType[value];
  }

}
