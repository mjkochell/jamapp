import { PianoService } from './../../core/piano.service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rootname'
})
export class RootnamePipe implements PipeTransform {

  constructor(private pianoService: PianoService)  {

  }

  transform(value: any, args?: any): any {
    return this.pianoService.getNoteByKeyId(value).fullname;
  }

}
