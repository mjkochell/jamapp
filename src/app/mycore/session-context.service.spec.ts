import { TestBed, inject } from '@angular/core/testing';

import { SessionContextService } from './session-context.service';

describe('SessionContextService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SessionContextService]
    });
  });

  it('should be created', inject([SessionContextService], (service: SessionContextService) => {
    expect(service).toBeTruthy();
  }));
});
