import { BehaviorSubject } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import {SessionStorageService} from 'ng2-webstorage';

@Injectable()
export class SessionContextService {
  private username: string;
  private idsocket: number;
  private leaveSessionSubject: BehaviorSubject<any>;
  private joinSessionSubject: BehaviorSubject<any>;
  private idsession: number;
  private idprog;

  constructor(private sessionStorage: SessionStorageService) {
    let context = sessionStorage.retrieve('context');
    if (context && context.idsession) {
      this.username = context.username;
      this.idsession = context.idsession;
      this.idprog = context.idprog;
    }
    else {
      this.username = '';
      this.idsession = null;
      this.idprog = null;
      let newcontext = {
        idsession: null,
        username: '',
        idprog: null,
      };
    this.sessionStorage.store('context', newcontext);
    }
    
    this.leaveSessionSubject = new BehaviorSubject<any>(false);
    this.joinSessionSubject = new BehaviorSubject<any>(false);
    
   }

   public getIdprog() {
    return this.idprog;
   }

   public setIdProg(idprog) {
    this.idprog = idprog;
    let context = this.sessionStorage.retrieve('context');
    context.idprog = idprog;
    this.sessionStorage.store('context', context);
   }

  public setUsername(username) {
    this.username = username;
    let context = this.sessionStorage.retrieve('context');
    context.username = username;
    this.sessionStorage.store('context', context);
  }

  public getUsername() {
    return this.username;
  }

  public setidSocket(idsocket) {
    this.idsocket = idsocket;
  }

  public getidSocket() {
    return this.idsocket;
  }

  public getContext() {
    return {
      username: this.username,
      idsocket: this.idsocket,
      idprog: this.idprog,
    };
  }

  public getLeaveSessionSubject() {
    return this.leaveSessionSubject;
  }

  public getJoinSessionSubject() {
    return this.joinSessionSubject;
  }

  public leaveSession() {
    let context = {
      idsession: null,
      username: '',
    };
    this.sessionStorage.store('context', context);
    
    this.leaveSessionSubject.next(true);
  }

  public joinSession() {
    this.joinSessionSubject.next(true);
  }

  public setidSession(idsession) {
    this.idsession = idsession;
    let context = this.sessionStorage.retrieve('context');
    context.idsession = idsession;
    this.sessionStorage.store('context', context);

  }

  public getidSession() {
    return this.idsession;
  }
}
