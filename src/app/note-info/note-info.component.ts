import { Chord } from './../mycore/model';
import { ProgressionService } from './../progression/progression.service';

import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { PianoNote } from '../core/piano-note';
import { PianoService } from '../core/piano.service';

@Component({
  selector: 'note-info',
  templateUrl: './note-info.component.html',
  styleUrls: ['./note-info.component.scss']
})
export class NoteInfoComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  currentNote: PianoNote;
  alternateNote?: PianoNote;
  title: string = "Play";

  @Input()
  chord: Chord;

  @Input()
  cardsize: string;

  constructor(private pianoService: PianoService, private progService: ProgressionService) {
    this.subscription = pianoService.notePlayed$.subscribe(
      pianoNote => {
        //  this.title = "Now playing";
        //  this.currentNote = pianoNote;
        //  this.alternateNote = this.pianoService.getAlternateNote(pianoNote.noteId);
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


  selectedChord(): void {
    // this.pianoService.playNote(this.currentNote.noteId);
    this.progService.selectedChord(this.chord);
  }

}
