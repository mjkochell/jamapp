import { PianoService } from './../core/piano.service';
import { ChordProgression } from './../mycore/model';
import { CurrentViewService } from 'app/routes/current-view.service';
import { SessionContextService } from 'app/mycore/session-context.service';
import { BehaviorSubject } from 'rxjs/Rx';
import { WebsocketOutgoingActionService } from 'app/web-socket/websocket-outgoingaction.service';
import { ProgressionService } from './progression.service';
import { APIService } from './../mycore/api';
import { Subscription } from 'rxjs/Subscription';
import { WebSocketService, IWSPacket } from './../web-socket/web-socket.service';
import { Injectable } from '@angular/core';


@Injectable()
export class CollectionService {
    private subscription: Subscription;
    private collectionSubject: BehaviorSubject<{ [key: number]: ChordProgression; }>;
    private progressions:  { [key: number]: ChordProgression; };
    private idsession: Number;


    constructor(
                private api: APIService,
                private progService: ProgressionService,
                private wsOutgoing: WebsocketOutgoingActionService,
                private context: SessionContextService,
                private currentView: CurrentViewService,
                private pianoService: PianoService,
                ) {
        this.collectionSubject = new BehaviorSubject({});
        this.context.getLeaveSessionSubject().subscribe(didLeave => {
            if (didLeave) {
                this.removeProgressions();
            }
        });
        this.context.getJoinSessionSubject().subscribe(didJoin => {
            if (didJoin) {
                this.initialize();
            }
        });
        this.progService.getUpdateSubject().subscribe(prog => {
            if (prog) {
                this.updateProgression(prog);
            }
        });

        
    }

    getCollectionSubject() {
        return this.collectionSubject;
    }

    initialize() {
        this.idsession = this.context.getidSession();
        this.api.getSession(this.idsession).subscribe(session => {
            this.progressions = session.progressions;
            
            Object.keys(this.progressions).map(key => {
                this.progressions[key] = new ChordProgression(this.progressions[key], this.pianoService);
                this.collectionSubject.next(this.progressions);
            });

            if (this.context.getIdprog()) {
                this.progService.setProgression(this.progressions[this.context.getIdprog()]);
            }
        });
    }

    getProgression(idprog) {
        return this.progressions[idprog];
    }

    createProgression() {
        this.api.createProgression().subscribe(data => {
            this.progressions[data.idprog] = new ChordProgression(data, this.pianoService);
            this.updateProgression(data);
            this.progService.setProgression(data);
            this.currentView.switchToEditProg();
        });
    }

    updateProgression(prog) {
        let idprog = prog.idprog;
        if (!(idprog in this.progressions)) {
            this.progressions[prog.idprog] = new ChordProgression(prog, this.pianoService);
            this.collectionSubject.next(this.progressions);
        }
        this.progressions[idprog].chords = prog.chords;
        if (this.progService.currentProgression.idprog === idprog) {
            this.progService.setProgression(prog);
        }
    }

    private removeProgressions() {
        this.progressions = {};
        this.collectionSubject.next(this.progressions);
    }

}
