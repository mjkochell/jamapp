import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressionViewComponent } from './progression-view.component';

describe('ProgressionViewComponent', () => {
  let component: ProgressionViewComponent;
  let fixture: ComponentFixture<ProgressionViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressionViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
