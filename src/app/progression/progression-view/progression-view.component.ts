import { CurrentViewService } from './../../routes/current-view.service';
import { SessionContextService } from './../../mycore/session-context.service';
import { APIService } from './../../mycore/api';
import { WebsocketOutgoingActionService } from 'app/web-socket/websocket-outgoingaction.service';
import { ChordProgression, Chord } from './../../mycore/model';
import { Subscription } from 'rxjs/Subscription';
import { ProgressionService } from './../progression.service';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';



@Component({
  selector: 'app-progression-view',
  templateUrl: './progression-view.component.html',
  styleUrls: ['./progression-view.component.scss']
})
export class ProgressionViewComponent implements OnInit, OnDestroy {
  private currentProgression: ChordProgression;
  private progSubscription: Subscription;
  private currentChord: Chord;
  private chordSubscription: Subscription;
  private chords: Chord[];

  @Input()
  private cardsize: string;

  constructor(
    private progService: ProgressionService,
    private wsOutgoing: WebsocketOutgoingActionService,
    private api: APIService,
    private context: SessionContextService,
    private currentView: CurrentViewService
    ) {
    this.chords = progService.currentProgression.chords;
  }

  ngOnInit() {
    this.progSubscription = this.progService
      .progressionWatcher.subscribe(prog => {
        this.currentProgression = prog;
        this.chords = prog.chords;
      });
    this.chordSubscription = this.progService
    .chordWatcher.subscribe(chord => { this.currentChord = chord; });
  }

  ngOnDestroy() {
    this.progSubscription.unsubscribe();
    this.chordSubscription.unsubscribe();
  }

  copyProg() {
    this.api.copyProg(this.currentProgression).subscribe(prog => {
      this.progService.setProgression(prog);
      this.currentView.switchToEditProg();      
    });
  }

  broadcast() {
    this.wsOutgoing.broadcast(this.currentProgression);
  }

  removeChord() {
    this.progService.removeChord();
  }



}
