import { TestBed, inject } from '@angular/core/testing';

import { ProgressionService } from './progression.service';

describe('ProgressionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProgressionService]
    });
  });

  it('should be created', inject([ProgressionService], (service: ProgressionService) => {
    expect(service).toBeTruthy();
  }));
});
