import { ScalesService } from './scales.service';
import { SessionContextService } from './../mycore/session-context.service';

import { WebsocketOutgoingActionService } from './../web-socket/websocket-outgoingaction.service';
import { APIService } from './../mycore/api';

import { Chord, ChordProgression, ChordType } from './../mycore/model';
import { WebSocketService, IWSPacket } from './../web-socket/web-socket.service';
import { PianoService } from './../core/piano.service';
import { PianoNote } from './../core/piano-note';
import { IPianoKey } from './../keyboard/ipiano-key';
import { Injectable } from '@angular/core';

import {Observable, BehaviorSubject} from 'rxjs/Rx';


@Injectable()
export class ProgressionService {
  public currentChord: Chord;
  public lazyChord: Chord;
  public chordWatcher: BehaviorSubject<Chord>;
  public currentProgression: ChordProgression;
  public progressionWatcher: BehaviorSubject<ChordProgression>;
  private updateSubject: BehaviorSubject<ChordProgression>;
  private settingScale;
  private scaleMode;

  // private websocketService: WebSocketService;

  constructor(
      private pianoService: PianoService,
      private apiService: APIService,
      private wsOutgoing: WebsocketOutgoingActionService,
      private context: SessionContextService,
      private scalesService: ScalesService) {
    this.currentChord = null;
    this.chordWatcher = new BehaviorSubject<Chord>(this.currentChord);
    this.currentProgression = new ChordProgression(null);
    this.progressionWatcher = new BehaviorSubject<ChordProgression>(this.currentProgression);
    this.updateSubject = new BehaviorSubject<ChordProgression>(null);

    // this.subscribeToSocket();

    this.scalesService.getScaleModeSubject().subscribe(isScaleMode => {
      this.settingScale = isScaleMode;
    });

    this.scalesService.getScaleconfigSubject().subscribe(tonic => {
      
    });

   }

   public removeChord() {
     if (this.currentChord) {
        let index = this.currentProgression.chords.indexOf(this.currentChord);
        this.currentProgression.chords.splice(index, 1);
        this.wsOutgoing.updateProgression(this.currentProgression);
        this.updateSubject.next(this.currentProgression);
     }
   }

   private confirmScale() {
    this.settingScale = false;
   }

   public getUpdateSubject() {
     return this.updateSubject;
   }

   private subscribeToSocket() {
    //  this.wsService.getSubject().subscribe((packet: IWSPacket) => {
    //    if (packet && packet.method === 'updateProgression') {
    //     this.setProgression(packet.prog);
    //    }

    //  });
   }

   public setProgression(prog) {
    //  this.pianoService
     this.currentProgression = new ChordProgression(prog, this.pianoService);
     this.progressionWatcher.next(this.currentProgression);
     this.currentChord = null;
     this.chordWatcher.next(this.currentChord);
   }

   public setNote(noteNumber: number) {
     // TODO: check if we are currently editing a chord
     if (this.settingScale) {
      this.scalesService.setRoot(noteNumber);
      return;
     }
     if (!this.currentChord) {
      let note = this.pianoService.getNoteByKeyId(noteNumber);
      this.lazyChord = new Chord({root: note, type: ChordType.Major});
      // this.chordWatcher.next(this.lazyChord);
      this.currentProgression.addChord(this.lazyChord);
      this.wsOutgoing.updateProgression(this.currentProgression);
      this.updateSubject.next(this.currentProgression);
      // this.coll
     }
     else {
       this.currentChord.setRoot(this.pianoService.getNoteByKeyId(noteNumber));
       this.wsOutgoing.updateProgression(this.currentProgression);
       this.updateSubject.next(this.currentProgression);
       this.unselectedChord();
     }

  }

  public unselectedChord() {
    this.currentChord = null;
    this.chordWatcher.next(this.currentChord);
    this.lazyChord = null;
  }

   public setChordType(type: ChordType) {
     // TODO: check if we are currently editing a chord
     if (this.settingScale) {
      this.scalesService.setScaleType(type);
      return;
     }
     let chord = this.currentChord || this.lazyChord;
     if(chord) { 
      chord.setChordType(type);
      this.wsOutgoing.updateProgression(this.currentProgression);
      this.updateSubject.next(this.currentProgression);
     }
     this.unselectedChord();
   }

   public selectedChord(chord: Chord) {
     if (this.settingScale) {
      let newchord = new Chord(chord);
      this.currentProgression.addChord(chord);
      this.wsOutgoing.updateProgression(this.currentProgression);
      this.updateSubject.next(this.currentProgression);
     }
     let tempChord = this.currentChord;
      if (tempChord) {
        this.unselectedChord();
      }
      if (!tempChord || tempChord !== chord)  {
        this.currentChord = chord;
        this.chordWatcher.next(chord);
        this.lazyChord = null;
      }
   }

}

