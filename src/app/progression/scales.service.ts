import { PianoService } from './../core/piano.service';

import { ChordType, Chord } from 'app/mycore/model';
import { BehaviorSubject } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
@Injectable()
export class ScaleCreator {
  private scaleDegrees;
  
  private MINOR = [
    {num: 0, type: ChordType.Minor},
    {num: 3, type: ChordType.Major},
    {num: 5, type: ChordType.Minor},
    {num: 7, type: ChordType.Minor},
    {num: 8, type: ChordType.Major},
    {num: 10, type: ChordType.Major}];

   private MAJOR = [
    {num: 0, type: ChordType.Major},
    {num: 2, type: ChordType.Minor},
    {num: 4, type: ChordType.Minor},
    {num: 5, type: ChordType.Major},
    {num: 7, type: ChordType.Major},
    {num: 9, type: ChordType.Minor}];
    
  // private MAJOR = [0, 2, 4, 5, 7, 9];
  
  constructor(private pianoService: PianoService) {
    this.scaleDegrees = {};
    this.scaleDegrees[ChordType.Major] = this.MAJOR;
    this.scaleDegrees[ChordType.Minor] = this.MINOR;
  }

  public create(tonic: Chord): Chord[] {
    let rootnum = tonic.root.keyId;
    let scaletype = tonic.type;

    let scale = this.scaleDegrees[scaletype]
      .map(degree => {
        let newindex = rootnum + degree.num;
        let root = this.pianoService.getNoteByKeyId(newindex);
        let type = degree.type;
        let chord = new Chord({
          root,
          type,
        });
        return chord;
      });
    
    return scale;
  }
}



@Injectable()
export class ScalesService {
  private scaleChordsSubject: BehaviorSubject<Chord[]>;
  private scaleModeSubject: BehaviorSubject<boolean>;
  private scaleMode: boolean;
  private root;
  private scaleType: ChordType;
  private scaleConfigSubject: BehaviorSubject<Chord>;
  private tonic: Chord;
  private fullscale: Chord[];

  constructor(
      private pianoService: PianoService,
      private scaleCreator: ScaleCreator,
    ) { 
    this.scaleMode = false;
    this.scaleModeSubject = new BehaviorSubject<boolean>(this.scaleMode);
    this.tonic = new Chord({
      root: this.pianoService.getNoteByKeyId(16),
      type: ChordType.Major,
    });

    this.scaleChordsSubject = new BehaviorSubject<Chord[]>([]);

    this.scaleConfigSubject = new BehaviorSubject<Chord>(this.tonic);
    this.scaleConfigSubject.subscribe(tonic => {
      this.createScaleFromTonic();
    });

    
  }

  private createScaleFromTonic() {
    this.fullscale = this.scaleCreator.create(this.tonic);
    if (this.fullscale) {
      this.scaleChordsSubject.next(this.fullscale);
    }
    
    console.log(this.fullscale);
  }
  
  public getScaleChordsSubject() {
    return this.scaleChordsSubject;
  }

  public getScaleModeSubject() {
    return this.scaleModeSubject;
  }

  public getScaleconfigSubject() {
    return this.scaleConfigSubject;
  }

  public setScaleMode(toSet) {
    this.scaleMode = toSet;
    this.scaleModeSubject.next(this.scaleMode);
  }

  public toggleScaleMode() {
    this.scaleMode = !this.scaleMode;
    this.scaleModeSubject.next(this.scaleMode);
  }

  public setRoot(num) {
    // this.root = num;
    this.tonic.setRoot(this.pianoService.getNoteByKeyId(num));
    this.scaleConfigSubject.next(this.tonic);
  }

  public setScaleType(type) {
    // this.scaleType = type;
    this.tonic.setChordType(type);
    this.scaleConfigSubject.next(this.tonic);
  }

}

