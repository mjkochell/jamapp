import { ProgressionService } from 'app/progression/progression.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable()
export class CurrentViewService {

  constructor(private router: Router) { }

  public switchToSelectProg() {

    this.router.navigateByUrl(`selectprog`);
  }

  public switchToViewProg() {
    this.router.navigateByUrl(`viewprog`);
  }

  public switchToEditProg() {
    this.router.navigateByUrl(`editprog`);

  }

}
