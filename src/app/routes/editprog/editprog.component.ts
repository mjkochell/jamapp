import { ChordType } from 'app/mycore/model';
import { PianoService } from './../../core/piano.service';
import { PianoNote } from './../../core/piano-note';
import { Chord } from './../../mycore/model';
import { ProgressionService } from 'app/progression/progression.service';
import { Subscription } from 'rxjs/Subscription';
import { ScalesService } from './../../progression/scales.service';
import { CurrentViewService } from './../current-view.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-editprog',
  templateUrl: './editprog.component.html',
  styleUrls: ['./editprog.component.scss']
})
export class EditProgComponent implements OnInit, OnDestroy {
    private scalechords: Chord[];
    private scaleChordsSubscription: Subscription;
    private scaletonic: Chord;
    private scaleTonicSubscription: Subscription;
  // private showPiano: boolean;

   private pianotoggler;
   private pickscaletoggler;
   private scaleroottoggler;
   private scalesModeSubcription: Subscription;
   private scaleroot: Chord;

  constructor(
    private currentView: CurrentViewService,
    private scalesService: ScalesService,
    private progressionService: ProgressionService,
    private pianoService: PianoService) {
    // this.showPiano = false;
    this.pianotoggler = {
      visible: true,
    };
    
    this.pickscaletoggler = {
      visible: false,
    };

    this.scaleroottoggler = {
      visible: false,
    };

   }
  
  ngOnInit() {
    this.scalesModeSubcription = this.scalesService.getScaleModeSubject()
      .subscribe(isScaleMode => {
        // this.pianotoggler.visible = !isScaleMode;
        // if (isScaleMode) {
          this.pickscaletoggler.visible = isScaleMode;
          this.scaleroottoggler.visible = isScaleMode;

        // }
      });   

    this.scaleTonicSubscription = this.scalesService.getScaleconfigSubject()
      .subscribe(tonic => {
        this.scaletonic = tonic;        
      });

      this.scaleChordsSubscription = this.scalesService.getScaleChordsSubject()
        .subscribe(chords => {
          this.scalechords = chords;
      });
  }

  ngOnDestroy() {
    this.scalesModeSubcription.unsubscribe();
  }

  goBack() {
    this.currentView.switchToSelectProg();
  }

  togglePiano() {
    // this.showPiano = !this.showPiano;
  }

}
