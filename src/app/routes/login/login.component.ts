import { WebsocketOutgoingActionService } from 'app/web-socket/websocket-outgoingaction.service';
import { WebSocketService } from './../../web-socket/web-socket.service';
import { APIService } from './../../mycore/api';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  private sessions: Array<Number>;
  private selectedsession: Number;
  private username: string;


  constructor(private apiService: APIService,
              private wsService: WebSocketService,
              private wsOutgoing: WebsocketOutgoingActionService) {
    this.username = '';
    this.sessions = [1, 2];
    this.selectedsession = this.sessions[this.sessions.length - 1];
   }

   login() {
     if (!this.username) {
       return;
     }
     this.wsOutgoing.joinSession(this.selectedsession, this.username);

   }

  ngOnInit() {

  }

  ngOnDestroy() {

  }

}
