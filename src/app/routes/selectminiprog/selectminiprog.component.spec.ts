import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectminiprogComponent } from './selectminiprog.component';

describe('SelectminiprogComponent', () => {
  let component: SelectminiprogComponent;
  let fixture: ComponentFixture<SelectminiprogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectminiprogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectminiprogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
