import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectprogComponent } from './selectprog.component';

describe('SelectprogComponent', () => {
  let component: SelectprogComponent;
  let fixture: ComponentFixture<SelectprogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectprogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectprogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
