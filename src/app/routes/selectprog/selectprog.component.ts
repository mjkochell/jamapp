import { CollectionService } from './../../progression/collection.service';
import { CurrentViewService } from 'app/routes/current-view.service';
import { ProgressionService } from 'app/progression/progression.service';
import { ChordProgression } from './../../mycore/model';
import { APIService } from './../../mycore/api';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';


@Component({
  selector: 'app-selectprog',
  templateUrl: './selectprog.component.html',
  styleUrls: ['./selectprog.component.scss']
})
export class SelectProgComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private sessionid: Number;
  private progressions: Array<ChordProgression>;

  constructor(private route: ActivatedRoute,
              private api: APIService,
              private progService: ProgressionService,
              private currentView: CurrentViewService,
              private progCollection: CollectionService) {
    this.progressions = [];
   }

   private gotoProg(prog) {
     this.progService.setProgression(prog);
     this.currentView.switchToViewProg();
   }

   private createProgression() {
    this.progCollection.createProgression();
   }

  ngOnInit() {
    this.subscription = this.progCollection.getCollectionSubject().subscribe(progs => {
      this.progressions = (<any>Object).values(progs);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
