import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewprogComponent } from './viewprog.component';

describe('ViewprogComponent', () => {
  let component: ViewprogComponent;
  let fixture: ComponentFixture<ViewprogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewprogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewprogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
