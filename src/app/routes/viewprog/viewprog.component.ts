import { CurrentViewService } from './../current-view.service';
import { Subscription } from 'rxjs/Subscription';
import { Chord } from './../../mycore/model';
import { ChordProgression } from 'app/mycore/model';
import { ProgressionService } from 'app/progression/progression.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-viewprog',
  templateUrl: './viewprog.component.html',
  styleUrls: ['./viewprog.component.scss']
})
export class ViewprogComponent implements OnInit, OnDestroy {
  private prog: ChordProgression;
  private chords: Array<Chord>;
  private subscription: Subscription;

  constructor(private progService: ProgressionService, private currentView: CurrentViewService) {
    this.prog = this.progService.currentProgression;
    this.chords = this.prog.chords;
   }

  ngOnInit() {
    this.subscription = this.progService.progressionWatcher.subscribe(prog => {
      this.prog = prog;
      this.chords = this.prog.chords;
    });
  }

  goBack() {
    this.currentView.switchToSelectProg();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
