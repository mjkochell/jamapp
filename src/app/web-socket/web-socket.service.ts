import { ChordProgression, IChordProgression, IChord } from './../mycore/model';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Rx';
import { BehaviorSubject } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';

// tslint:disable-next-line:no-empty-interface
export interface IWSPacket {
  method: string;
  prog: IChordProgression;

}

@Injectable()
export class WebSocketService {
  private subject: BehaviorSubject<IWSPacket>;
  private connectedSubject: BehaviorSubject<boolean>;
  private socket: Observable<MessageEvent>;
  private socketSubscription: Subscription;

  private url: string;
  private ws: any;
  private username: string;

  constructor() {
    this.subject = new BehaviorSubject<IWSPacket>(null);
    this.connectedSubject = new BehaviorSubject<boolean>(false);
    this.url = '/socket.io/';
  }

  public getSubject(): BehaviorSubject<IWSPacket> {
    return this.subject;
  }

  public getConnectedSubject() {
    return this.connectedSubject;
  }

  public connect(): Observable<MessageEvent> {
    if (!this.socket) {
      this.socket = this.create(this.url);
    }
    return this.socket;
  }

  private create(url): Observable<MessageEvent> {
    let ws = io({ 'path': url });
    let subject = this.subject;
    let observable: Observable<MessageEvent> = Observable.create(
        (obs) => {
            ws.onmessage = obs.next.bind(obs);
            ws.onerror = obs.error.bind(obs);
            ws.onclose = obs.complete.bind(obs);
            return ws.close.bind(ws);
        }
    );
    this.socketSubscription = observable.subscribe(
      (msg: MessageEvent) => {
        let data: IWSPacket = JSON.parse(msg.data);
        subject.next(data);
    });

    this.ws = ws;

    console.log('CONNECTING');
    
    ws.on('connect', () => {
      this.connectedSubject.next(true);
      console.log('CONNECTED');
      ws.on('message', (data) => {
        subject.next(data);
      });

    });

    return observable;

  }


  public send(data) {
    let socket = this.ws;
    socket.emit('message', data);
    console.log('sending');
    console.log(data);
  }

}
