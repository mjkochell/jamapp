import { TestBed, inject } from '@angular/core/testing';

import { WebsocketActionService } from './websocket-action.service';

describe('WebsocketActionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WebsocketActionService]
    });
  });

  it('should be created', inject([WebsocketActionService], (service: WebsocketActionService) => {
    expect(service).toBeTruthy();
  }));
});
