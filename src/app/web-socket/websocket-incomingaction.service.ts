import { CollectionService } from 'app/progression/collection.service';

import { CurrentViewService } from './../routes/current-view.service';
import { Injectable } from '@angular/core';
import { WebSocketService } from 'app/web-socket/web-socket.service';
import { ChordProgression } from 'app/mycore/model';
import { ConfigService } from 'app/mycore/config.service';
import { SessionContextService } from 'app/mycore/session-context.service';
import { ProgressionService } from 'app/progression/progression.service';

@Injectable()
export class WebsocketIncomingActionService {
  private methods;


  constructor(private wsService: WebSocketService,
              private config: ConfigService,
              private progService: ProgressionService,
              private currentView: CurrentViewService,
              private progCollection: CollectionService,
              private userContext: SessionContextService,
              ) {
    this.wsService.getSubject().subscribe(data => {
      if (data) {
        console.log(data);
        this.doAction(data);
      }
      
    });

   }

   private doAction(data) {
     let methods = {
      broadcast: this.broadcast.bind(this),
      updateProgression: this.updateProgression.bind(this),
      assignSocket: this.assignSocket.bind(this),
    };
    if(!data) {
      return;
    }
    if (data.method in methods) {
      let method = methods[data.method];
      method(data);
    }
   }

   private assignSocket(data) {
    let idsocket = data.idsocket;
    this.userContext.setidSocket(idsocket);
   }

   private updateProgression(data) {
     let prog = data.prog;
    this.progCollection.updateProgression(prog);
   }

   private broadcast(data) {
     this.progService.setProgression(data.prog);
      this.currentView.switchToViewProg();
     console.log(`incoming broadcast ${data}`);
   }
}
