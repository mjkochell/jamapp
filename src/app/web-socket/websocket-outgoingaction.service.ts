// import { CollectionService } from 'app/progression/collection.service';
import { Injectable } from '@angular/core';

import { CurrentViewService } from './../routes/current-view.service';

import { WebSocketService } from 'app/web-socket/web-socket.service';
import { ChordProgression } from 'app/mycore/model';
import { ConfigService } from 'app/mycore/config.service';
import { SessionContextService } from 'app/mycore/session-context.service';

@Injectable()
export class WebsocketOutgoingActionService {
  private methods;

  constructor(
            private wsService: WebSocketService,
              private context: SessionContextService,
              private currentView: CurrentViewService,
              // private collection: CollectionService
              ) {
    this.wsService.getSubject().subscribe(data => {

    });
    this.wsService.getConnectedSubject().subscribe(data => {
      if (data) {
        if (this.context.getidSession()) {
          this.joinSession(this.context.getidSession(), this.context.getUsername());
        }
      }
    });
   }

   public joinSession(idsession, username) {
    this.context.setUsername(username);
    this.context.setidSession(idsession);
    let sessioninfo = {idsession};
    let data = {
      username, sessioninfo,
      method: 'joinSession',
    };
    this.wsService.send(data);
    this.context.joinSession();
    this.currentView.switchToSelectProg();
   }

   public leaveSession(idsession, username) {
    let sessioninfo = {idsession};
    let data = {
      username, sessioninfo,
      method: 'leavesession',
    };
    this.wsService.send(data);
    this.context.leaveSession();
   }

   public updateProgression(prog: ChordProgression) {
    let data = {
      prog,
      method: 'updateProgression',
    };
    // this.collection.updateProgression(prog);

    this.wsService.send(data);
  }

   public broadcast(prog: ChordProgression) {
    let data = {
      method: 'broadcast',
      prog
    };
    this.wsService.send(data);
   }

}




