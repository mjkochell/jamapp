const express = require('express');
const bodyParser = require('body-parser');




module.exports = (app, masterSessionCollection, jamSessionFactory) => {

  const router = express.Router();
  // const sessionRoute = router.route('sessions');
  router.use(bodyParser.json());  

  router.get('/sessions/short', function(req, res) {
    var infos = masterSessionCollection.getShorts();
    return res.json(infos);
  })

  .post('/sessions', function(req, res) {
    let jamSession = jamSessionFactory.create();
    masterSessionCollection.addJamSession(jamSession);
    return res.json(jamSession);
  })

  .param('id', function(req, res, next, id){
    req.id = id;
    next();
  })

  .get('/sessions/:id', function(req, res) {
      let idsession = req.id;
      let jamSession = masterSessionCollection.getJamSession(idsession);
      return res.json(jamSession.getData());
    })

  .post('/sessions/:id/addProgression', function(req, res) {
      let session = masterSessionCollection.getJamSession(req.id);
      let idsocket = req.body.idsocket;
      let prog = session.createProgression(idsocket);
      return res.json(prog);
  })

  .post('/sessions/:id/copyProg', function(req, res) {
      let session = masterSessionCollection.getJamSession(req.id);
      let prog = req.body.prog;
      let idsocket = req.body.idsocket;
      
      let newProg = session.copyProgression(prog, idsocket);
      return res.json(newProg);
  })

  

  .put('/progressions/:id/', function(req, res) {
      
  })

  .put('/sessions/:id/broadcast', function(req, res) {
    let idsession = req.id;
    let jam = masterSessionCollection.getJamSession(idsession);
    // let username = req.body.username;
    let socket = {idsocket: 45, username: 'API'};
    let prog = req.body.prog;
    jam.broadcast(socket, prog);
    return res.json({success: true});
  }); 

app.use(router);

};