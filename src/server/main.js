module.exports = (app, server) => {
    const model = require('./model');
    const JamSessionFactory = model.JamSessionFactory,
          JamSessionCollection = model.JamSessionCollection;
    const jamSessionFactory = new JamSessionFactory();
    const masterSessionCollection = new JamSessionCollection(jamSessionFactory);
    
    const wsservermodule = require('./wsserver');
    let wsserver = wsservermodule(server, masterSessionCollection);

    const apiroutes = require('./apiroutes');
    apiroutes(app, masterSessionCollection, jamSessionFactory);
};