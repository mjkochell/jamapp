
var sessions = {};
// exapp.post('/pubevent', function(req, res){
//     var idvuser = parseInt(req.body.idvuser);
//     if(sockets[idvuser]) {
//         sockets[idvuser].emit('data', req.body);
//         res.status(200);
//     	res.end();
//     	console.log('got the socket');
// 	}
// 	else {
// 		res.status(404);
//     	res.end();
//     	console.log('dont got the socket');
// 	}
// });

const chordTypes = {
  'Major': 0,
  'Minor': 1,
};

// var sessionSockets = {
//   1: {
//     jimmy: {
//       mysocket: 'stuff',
//       idsocket: 3,
//     }
//   }
// }

var currentAtomicSession = 2;
var sessionData = {
  1: {
    idsession: 1,
    currentAtomicProgression: 1,
    progressions: {
      1:
      {
        id: 1,
        chords: [
          {
            root: 24,
            type: chordTypes.Major,
          }
        ]
      }
    },
  },
  2: {
    idsession: 2,
    currentAtomicProgression: 1,
    progressions: {
      1:
      {
        id: 1,
        chords: [
          {
            root: 26,
            type: chordTypes.Minor,
          }
        ]
      }
    },
  },
};

class Chord {

  constructor(root, chordType) {
    this.root = root;
    this.chordType = chordType;
  }
}


class Progression {
  // idprog;
  // idsession;
  // chords;
  constructor(idprog, idsession, chords) {
    this.idprog = idprog;
    this.idsession = idsession;
    this.chords = chords || [];
  }
  updateChords(chords) {
    this.chords = chords;
  }
}


class JamSessionData {
  // idsession;
  // progressions;
  // currentAtomicProgression;
  
  constructor(idsession, sessiondata) {
    this.idsession = idsession;
    if(sessiondata) {
      this.progressions = sessiondata.progressions;
      this.currentAtomicProgression = sessiondata.currentAtomicProgression;
    }
    else {
      this.currentAtomicProgression = 0;
      this.makefake();
    }
  }

  makefake() {
    this.progressions = {};
    // idprog, idsession, chords
    let prog = this.createProgression();
    let chords = [
      new Chord(16, chordTypes.Major),
      new Chord(18, chordTypes.Minor),
    ];
    prog.updateChords(chords);
  }

  createProgression() {
    let idprog = this.currentAtomicProgression = ++this.currentAtomicProgression;
    let prog = new Progression(idprog, this.idsession);
    this.progressions[idprog] = prog;
    return prog;
  }

  updateProgression(prog) {
    console.log(prog);
    let idprog = prog.idprog;
    let myprog = this.progressions[idprog];
    myprog.updateChords(prog.chords);
  }
}

class SessionShort {
  // idsession;
  // users;
  constructor(idsession, active, users) {
    this.idsession = idsession;
    this.active = active;
    this.users = users && users.length ? users : [];
  }
}

class JamSession {
  // idsession;
  // sockets;
  // active;
  // sessiondata;
  constructor(idsession, sessiondata) {
    this.idsession = idsession;
    this.sockets = {};
    this.active = false;
    this.sessiondata = new JamSessionData(this.idsession, sessiondata);
  }

  getData() {
    return this.sessiondata;
  }

  getShort() {
    if(!this.active) {
      console.log('getting shorts, jam session ' + this.idsession + ' not active.');
    }
    else {
      console.log('getting shorts, jam session ' + this.idsession + ' is active.');
    }
      var users = Object.keys(this.sockets);
      console.log('current users:');
      console.log(users);
      return new SessionShort(this.idsession, this.active, users);
  }

  joinSession(socket) {
    if(socket.username in this.sockets) {
      console.log('new jam session socket already exists for user ' + socket.username);
    }
    if(this.active) {
      // TODO: tell users you have joined the session
    }
    console.log('joined the session');
    this.active = true;
    this.sockets[socket.username] = socket;
    // console.log(this.sockets);
  }

  leaveSession(socket) {
    delete this.sockets[socket.username];
    if(Object.keys(this.sockets).length !== 0) {
      this.active = false;
    }
    else {
      // TODO: tell users you have left the session
    }
  }

  createProgression(idsocket) {
    var prog = this.sessiondata.createProgression();
    this.updateProgression({idsocket: null}, prog);
    return prog;
  }

  copyProgression(prog, idsocket) {
    let idprog = prog.idprog;
    let newProg = this.createProgression();
    newProg.chords = prog.chords;
    this.updateProgression({idsocket: null}, newProg);
    return newProg;
  }

  updateProgression(socket, prog) {
    this.sessiondata.updateProgression(prog);
    
    Object.keys(this.sockets).forEach((key) => {
      let othersocket = this.sockets[key];
      console.log(othersocket.username);
      console.log(Object.keys(this.sockets).length);
      if (othersocket.idsocket !== socket.idsocket) {
        console.log('\n\n');
        console.log('HERE SENDING');
        let data = {
          method: 'updateProgression',
          prog,
        };
        othersocket.emit('message', data);
        console.log('DONE EMITTING');
      }
      else {
        console.log(`skipping over sender in jam session  ${othersocket.username} = ${socket.username}`);
      }
    });
  }

  broadcast(socket, prog) {
    Object.keys(this.sockets).forEach((key) => {
      let othersocket = this.sockets[key];
      if (othersocket.idsocket !== socket.idsocket) {
        let data = {
          method: 'broadcast',
          prog,
        };
        othersocket.emit('message', data);
      }
      else {
        console.log(`skipping over sender in jam session  ${othersocket.username} ${socket.username}`);
      }
    });
  }

  isRunning() {
    return this.active;
  }

}

class JamSessionCollection {
  // jamSessions;
  constructor(jamSessionFactory, fname) {
    this.jamSessionFactory = jamSessionFactory;
    if(fname) {
      this.loadfile(fname);
    }
    else {
      this.makefake();
    }
    
  }

  loadfile(fname) {
    this.jamSessions = {};
  }

  makefake() {
    this.jamSessions = {};
    let newjam = this.jamSessionFactory.create();
    this.addJamSession(newjam);
  }

  addJamSession(session) {
    let idsession = session.idsession;
    this.jamSessions[idsession] = session;
  }

  getJamSession(idsession) {
    let session = this.jamSessions[idsession];
    return session;
  }

  getShorts() {
    let jams = Object.values(this.jamSessions);
    let shorts = jams.map(jam => jam.getShort());
    return shorts;
  }

}

class JamSessionFactory {

  constructor(currentAtomicSession) {
    this.currentAtomicSession = currentAtomicSession || 1;
  }

  create() {
    let idsession = ++this.currentAtomicSession;
    let jamSession = new JamSession(idsession);
    return jamSession;
  }

}

module.exports = {
  JamSessionCollection,
  JamSession,
  JamSessionFactory,
};

