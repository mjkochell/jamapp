var express = require('express'),
	http = require('http');

let config = {
  ws: {
    port: 8080
  }
};

module.exports = function(server, masterSessionCollection) {

var io = require('socket.io').listen(server);
var bodyParser = require('body-parser');

let currentAtomicSocketID = 3;

let allSockets = {
  jimmy: {
    mysocket: 'info',
    idsocket: 3,
    username: 'jimmy',
  }
};

let allSocketsByID = {
 3: {
    mysocket: 'info',
    idsocket: 3,
    username: 'jimmy',
 }
};

function updateProgression(socket, data) {
  let prog = data.prog;
  let idsession = prog.idsession;
  let session = masterSessionCollection.getJamSession(idsession);
  if (session) {
    session.updateProgression(socket, prog);
  }
}

function joinSession(socket, data) {
  console.log('authenticating');
  socket.username = data.username;
  allSockets[socket.username] = socket;
  socket.idsession = data.sessioninfo.idsession;
  masterSessionCollection.getJamSession(data.sessioninfo.idsession)
    .joinSession(socket);
}

function leaveSession(socket, data) {
  masterSessionCollection.getJamSession(socket.idsession)
    .leaveSession(socket);
  socket.idsession = null;
}

function broadcast(socket, data) {
  console.log(data);
  masterSessionCollection.getJamSession(socket.idsession)
    .broadcast(socket, data.prog);
}

let methods = {
  joinSession,
  leaveSession,
  updateProgression,
  broadcast,
};

function initSocket(socket, data) {
  socket.emit('message', {
    method: 'assignSocket',
    idsocket: socket.idsocket,
  });
  socket.on('message', (data) => {
    let method = methods[data.method];
    if(method) {
      method(socket, data);  
    }
    else {
      console.log('no method provided');
    }
    
  });

  socket.on('disconnect', () => {
    if (socket.idsession) {
      masterSessionCollection.getJamSession(socket.idsession)
        .leaveSession(socket);
    }
    delete allSocketsByID[socket.idsocket];
    delete allSockets[socket.username];
  });
  
  
  allSocketsByID[socket.idsocket] = socket;
}

io.sockets.on('connection', (socket) => {
  socket.idsocket = ++currentAtomicSocketID;
  initSocket(socket);
});

console.log('wsserver listening');

};
